#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <error.h>
#include <fcntl.h>

int main()
{	
	int mififo1;
	char buf[100];

	int status = mkfifo("/tmp/mififo1",0777);
		
	if(status == -1)
	{
		perror("Fallo fifo");
		
	}
	mififo1 = open("/tmp/mififo1",O_RDONLY);	
	
	if(mififo1 == -1)
	{
		perror("Fallo open");
		
	}
	
	
	for(;;)
	{
		read(mififo1,buf,sizeof(buf));
		printf("%s\n",buf);
		
	 }

	
	status=close(mififo1);
	
}
