#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include "DatosMemCompartida.h"

int main()
{
	int fd;
	char *dir;
	
	DatosMemCompartida *pdata;
	// DatosMemCompartida datos;

	fd=open("/tmp/datos.txt", O_CREAT| O_RDWR,0666);

	if (fd==-1) {
		perror("open");
		exit(1);
	}
	
	 // Se proyecta el fichero 
	dir=(char *)mmap(NULL,sizeof(pdata), PROT_WRITE | PROT_READ , MAP_SHARED, fd, 0);
	
	close(fd);
	
	pdata =(DatosMemCompartida *)dir;
	
	
	if(pdata==NULL)
	perror("puntero vacio\n"); 
	
	for(;;)
	{
		usleep(25000);
		if(pdata->esfera.centro.y > pdata->raqueta1.y2)
			pdata->accion=1;
		else 
		if(pdata->esfera.centro.y < pdata->raqueta1.y1)
			pdata->accion=-1;
		else 
			pdata->accion=0;
	}
	
	
}
