// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

int fd;
CMundo::CMundo()
{
	Init();
	contador=0;
	
}

CMundo::~CMundo()
{	unlink("/tmp/datos.txt");
	close(FIFO);
	unlink("/tmp/FIFO");
	close(FIFO_COR);
	unlink("tmp/FIFO_COR");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char buffer[100]="";
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	/*Método que realiza la disminución del radio
	 de la practica*/
	if(contador%500==0)
	{
		if(esfera.GetRadio()>0.15f){
			esfera.SetRadio(esfera.GetRadio()-0.1f);
		}
	}
	
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		esfera.SetRadio(0.5f);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		esfera.SetRadio(0.5f);
		
	}
	//Incremento contador
	contador++;
	
	//Inicializacion datosmem
	pdatos->raqueta1=jugador2;
	pdatos->esfera=esfera;
	
	//Fin del juego
	if(puntos2==3)
	{
		fprintf(stdout,"Jugador 2 ha ganado el juego\n");
		exit(0);
	}
	if(puntos1==3)
	{
		fprintf(stdout,"Jugador 1 ha ganado el juego\n");
		exit(0);
	}
	
	if(pdatos==NULL)
	fprintf(stdout,"puntero vacio\n");
	
	//control bot
	if(pdatos->accion == 1)
		OnKeyboardDown('o',1,1);
	else 
	if (pdatos->accion == -1)
		OnKeyboardDown('l',1,1);
	char cadena[200];
	int status;
	status=read(FIFO,cadena,sizeof(cadena));
	sscanf(cadena,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
	

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	char cad[100];
	sprintf(cad,"%c",key);
	int s;
	s=write(FIFO_COR,cad,sizeof(cad));
	if(s==-1)perror("ERROR ESCRITURA COORDENADAS");
	//case 'a':jugador1.velocidad.x=-1;break;
	//case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	
	//Creacion fichero con memoria compartida
	int fd;

	char *addr;
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	if ((fd=open("/tmp/datos.txt",O_CREAT | O_TRUNC | O_RDWR, 0666))<0) {
		perror("No puede abrirse el fichero");
			exit(1);
	} 
	
	write(fd,&datosmem,sizeof(datosmem));
	
	
	 /* Se proyecta el fichero */
	addr=(char *)mmap(NULL,sizeof(datosmem), PROT_WRITE | PROT_READ , MAP_SHARED, fd, 0);
      
	close(fd);
	pdatos=(DatosMemCompartida *)addr;
	
	//Creacion tuberi FIFO
	// FIFO=mkfifo("/tmp/FIFO",0777);
	mkfifo("/tmp/FIFO",0777);
	// if(FIFO==-1)perror("ERROR CREACION FIFO");
	mkfifo("/tmp/FIFO_COR",0777);
	FIFO=open("/tmp/FIFO",O_RDONLY);
	if(FIFO==-1)perror("ERROR lectura FIFO");
	
	//Creacion tuberi FIFO_COR
	//FIFO_COR=mkfifo("/tmp/FIFO_COR",0777);
	
	//if(FIFO_COR==-1)perror("ERROR CREACION FIFO_COR");
	
	FIFO_COR=open("/tmp/FIFO_COR",O_WRONLY);
	if(FIFO_COR==-1)perror("ERROR lectura FIFO_COR");

	pdatos -> accion = 0;
}
