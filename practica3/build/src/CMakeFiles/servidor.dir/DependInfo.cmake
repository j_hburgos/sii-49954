# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/juan/Escritorio/practica3/src/Esfera.cpp" "/home/juan/Escritorio/practica3/build/src/CMakeFiles/servidor.dir/Esfera.cpp.o"
  "/home/juan/Escritorio/practica3/src/MundoServidor.cpp" "/home/juan/Escritorio/practica3/build/src/CMakeFiles/servidor.dir/MundoServidor.cpp.o"
  "/home/juan/Escritorio/practica3/src/Plano.cpp" "/home/juan/Escritorio/practica3/build/src/CMakeFiles/servidor.dir/Plano.cpp.o"
  "/home/juan/Escritorio/practica3/src/Raqueta.cpp" "/home/juan/Escritorio/practica3/build/src/CMakeFiles/servidor.dir/Raqueta.cpp.o"
  "/home/juan/Escritorio/practica3/src/Vector2D.cpp" "/home/juan/Escritorio/practica3/build/src/CMakeFiles/servidor.dir/Vector2D.cpp.o"
  "/home/juan/Escritorio/practica3/src/servidor.cpp" "/home/juan/Escritorio/practica3/build/src/CMakeFiles/servidor.dir/servidor.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
