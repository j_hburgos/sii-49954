// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////
#pragma once

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera();
	virtual ~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	Vector2D aceleracion;
	Vector2D posicion;
	float radio;
		
	void Mueve(float t);
	void Dibuja();
	float SetRadio(float rad);
	float GetRadio();
};
